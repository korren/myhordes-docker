# MyHordes Docker Wrapper

This wrapper can be used to run MyHordes with docker containers.

**Important:** This setup is designed for development purposes. Therefore it is optimized towards being easy
to debug and **not** for stability, speed or security. Never use this setup to run MyHordes on a public server!

## Setting up the environment

### Windows

Using this environment on Windows requires at least Windows 10 2004. Windows 11 is supported, as well.

**Make sure that Hardware Virtualisation is enabled within your BIOS/UEFI, otherwise WSL2 and Docker won't work (Intel VT-d or AMD-V)**

1. Enable the Windows Subsystem for Linux (WSL) and the Virtual Machine platform. Run the following code in a PowerShell terminal with elevated rights.
~~~powershell
dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart
dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart
~~~
2. Reboot your PC.
3. Set the default WSL version to v2 by running the following code in a PowerShell terminal.
~~~powershell
wsl --set-default-version 2
~~~
4. Once set, install the Kernel Update [here](https://docs.microsoft.com/windows/wsl/wsl2-kernel)
5. Install Debian from the Windows Store: [here](ms-windows-store://pdp/?ProductId=9MSVKQC78PK6)
6. Run Debian and follow the first-launch setup steps.
7. Download and install the latest version of Docker Desktop: https://docs.docker.com/desktop/windows/wsl/
   1. Check _Use WSL2 instead of Hyper-V (Recommended)_
8. Open Docker Desktop and verify/adjust the following settings:
   1. [General] _Use the WSL2 based engine_ `ON`
   2. [General] _Use Docker Compose V2_ `ON`
   3. [Resources / WSL Integration] _Enable integration with additional distros_ `Debian: On`
9. Within Debian, install `git`
~~~bash
sudo apt update
sudo apt install git
~~~
10. Within Debian, check out this repository into a location of your choice and continue with **Setting up the repository**.

### Ubuntu / Debian
TODO

## Setting up the repository

1. Install cURL (if not already installed)
~~~bash
sudo apt install curl
~~~

2. Clone repository and initialize git submodules: 
~~~bash
git clone git@gitlab.com:eternaltwin/myhordes/myhordes-docker.git
cd myhordes-docker
git submodule update --init --recursive
~~~
Alternatively, if you do not have a GitLab account with an associated SSH key, replace the first line with
~~~bash
git clone https://gitlab.com/eternaltwin/myhordes/myhordes-docker.git
~~~
3. Start the containers with `docker-compose up -d`
4. Install composer and all MyHordes project dependencies locally
~~~bash
tools/composer install
tools/yarn install
tools/yarn build
~~~

## Developing

### Operating the MyHordes server

#### Starting the server

Run the servers in the current terminal:
~~~bash
docker-compose up
~~~
Run the servers detached the current terminal:
~~~bash
docker-compose up -d
~~~

#### Stopping the server
If the server is running in the current terminal, you can stop it simply by cancelling the command (`CTRL + C`).
Otherwise, use the following command to stop a detached server
~~~bash
docker-compose down
~~~
To stop the server and delete the database at the same time, run
~~~bash
docker-compose down --volumes
~~~

### Initialize the installation

Either restore a previous database backup or initialize the game database by following the MyHordes Readme section
**Populate the database**.

### Domains and Services
| Service              | Domain                       | Port |
|----------------------|------------------------------|------|
| MyHordes Game Server | `myhordes.localhost`         | 80   |
| PHPMyAdmin           | `pma.myhordes.localhost`     | 80   |
| Mailhog              | `mailhog.myhordes.localhost` | 80   |
| Traeffic Dashboard   | `localhost`                  | 8080 |
| MariaDB              | `localhost`                  | 3306 |

### Tools

| Tool             | Description                                                                                                           |
|------------------|-----------------------------------------------------------------------------------------------------------------------|
| `tools/composer` | Runs the `composer` command within the PHP container. All input arguments are propagated into the container.          |
| `tools/console`  | Runs Symfony's `bin/console` command within the PHP container. All input arguments are propagated into the container. |
| `tools/yarn`     | Runs the `yarn` command within the PHP container. All input arguments are propagated.                                 |
| `tools/npm`      | Runs the `npm` command within the PHP container. All input arguments are propagated.                                  |
| `tools/php`      | Shortcut to executing code in the PHP container.                                                                      |
